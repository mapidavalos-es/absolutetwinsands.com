<?
include "../../config.php";

$client_id = filter_input(INPUT_GET, 'client_id', FILTER_VALIDATE_INT);
$lnk = filter_input(INPUT_GET, 'lnk', FILTER_SANITIZE_SPECIAL_CHARS);
$contract_number = filter_input(INPUT_GET, 'contract_number', FILTER_SANITIZE_SPECIAL_CHARS);

$params = array(
    'table' => 'absolute',
    'param' => 'interested',
    't' => $lnk
);

//$res = getCurlResult('register_email_status/add_param.php', $params);

/*andrey solicito que solo se envie contract number, no hace falta otros datos. */
// $params = array('client_id' => $client_id);
// $client_details = getCurlResult('absolute_destinations_get_member_details', $url_webservice, $params );

$html =
"I’m a Absolute Twin Sands Phase 3 Owner. My contract number is <strong>" . $contract_number . "</strong> . Please can you contact me with a view to arranging my stay in the completed 3rd Phase as of the 1st July onwards.”";

$params =
    array(
        //'to'           => 'reservationhub@absolute-destinations.com',
        'to'           => 'mapi@absoluteworld.net',
        'from_address' => 'robot@absolutemailservices.net',
        'from_name'    => 'Absolutetwinsands',
        'subject'      => 'My stay in the completed 3rd Phase ',
        'message'      => $html,
        'addTemplateBasic' => 1

    )
;

getCurlResult('api/?api=email&method=sendGeneralEmail', $params);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Absolute Twin Sands Resort & Spa Progress Update</title>

<style type="text/css">
@charset "utf-8";
body {
	background: #FFF;
	font-family: Arial, Helvetica, sans-serif;
	background-color: #eceeee;
}

a:link {
	text-decoration: none;
	color: #666666;
}
a:visited {
	text-decoration: none;
	color: #666666;
}
a:hover {
	text-decoration: none;
	color: #666666;
}
a:active {
	text-decoration: none;
	color: #666666;
}



.textlink a:link {
	text-decoration: none;
	color: #ccc;
}
.textlink a:visited {
	text-decoration: none;
	color: #ccc;
}
.textlink a:hover {
	text-decoration: none;
	color: #ccc;
}
.textlink a:active {
	text-decoration: none;
	color: #ccc;
}

</style>


</head>

<body style="background-color:#eceeee; margin-top:0px;margin-bottom:0px;margin-right:0px;margin-left:0px;font-family:Arial, Helvetica, sans-serif; line-height:24px; font-size:18px;" >
<table width="800" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr >
    <td valign="top"><img style="display:block;" src="images/header-thankyou.jpg" /></td>
   </tr>
   <tr>
      <td><img style="display:block;" src="images/thankyou.jpg" /></td>
  </tr>

  <tr>
        <td style="padding-left:40px; padding-right:27px; color:#FFF; background-color:#71868c; font-size:16px;">
        
            <table width="100%" border="0" align="center" style="font-family:Arial, Helvetica, sans-serif; color:#FFF; line-height:24px;">
              <tr>
              <td width="680" colspan="5" height="30" valign="top" >&nbsp;</td>
              </tr>
              <tr>
                <td width="16%">Australia</td>
                <td width="20%">+61 280 734452</td>
                <td width="20%" rowspan="5" align="center"><img style="display:block;" src="images/line.jpg" /></td>
                <td width="21%">Russia</td>
                <td width="23%">+7 499 795 7549</td>
              </tr>
                <tr>
                <td>China</td>
                <td>+86 400 8980 898</td>
                <td>South Africa</td>
                <td>+27 2130 01943</td>
              </tr>
              <tr>
                <td>Finland</td>
                <td>+35 894 2704142</td>
                <td>Thailand</td>
                <td>+66 762 92525</td>
              </tr>
              <tr>
                <td>France</td>
                <td>+33 176 773993</td>
                <td>UK</td>
                <td>+44 203 6423203</td>
              </tr>
              <tr>
                <td>Germany</td>
                <td>+49 692 222 1557</td>
                <td>USA</td>
                <td>+1 6465 645 654</td>
              </tr>
            
              </table>
        </td>
    </tr>
  <tr>
  <td>
  <img style="display:block;" src="images/follow-us.jpg" height="60" border="0" />
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
      <tr><td><img style="display:block;" src="images/spacer.jpg" width="205" height="48"></td>
      
      <td><a href="https://www.facebook.com/absolutetwinsands/" target="_blank" ><img style="display:block;" src="images/FB.jpg" width="65" height="48" /></a></td>
      <td><a href="https://twitter.com/AbsoluteTwSands" target="_blank" ><img style="display:block;" src="images/TW.jpg" width="60" height="48" /></a></td>
      <td><a href="https://vimeo.com/absoluteworld" target="_blank" ><img style="display:block;" src="images/vimeo.jpg" width="64" height="48" /></a></td>
      <td><a href="https://www.pinterest.com/absolutetwsands/" target="_blank" ><img style="display:block;" src="images/pin.jpg" width="63" height="48" /></a></td>
      <td><a href="https://plus.google.com/113029068237829819150/posts" target="_blank" ><img style="display:block;" src="images/google.jpg" width="58" height="48" /></a></td>
      <td><a href="http://www.youtube.com/user/absoluteworldgroup" target="_blank" ><img style="display:block;" src="images/YT.jpg" width="80" height="48" /></a></td>
      <td><img style="display:block;" src="images/spacer.jpg" width="205" height="48"></td></tr>
      </table>
  </td>
  </tr>
  <tr><td><a href="http://www.absolutetwinsands.com" ><img style="display:block;" src="images/absolutetwinsands.jpg" /></a></td></tr>
</table>


</body>
</html>


